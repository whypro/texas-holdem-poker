# -*- coding: utf-8 -*-
from __future__ import division
from card import Card
from hand import Hand, StartingHand


class PlayerBout(object):
    def __init__(self, flag):
        self.flag = flag
        self.hole_cards = []
        self.pot = 0      # 当前玩家下注总额
        self.fold = False
        self.actions = []
        self.rank = 0
        self.nut_hand = None

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self.__dict__)


class Player(object):

    def __init__(self, player_id):
        self.id = player_id
        self.jetton = 0
        self.money = 0
        self.historys = []
        self.current_bout = None
        self.dead = False

        # action_map['pre-flop']['raise'] = cards_level
        self.action_map = {}

    def begin_bout(self, jetton, money, flag):
        self.dead = False
        self.update(jetton, money)
        self.current_bout = PlayerBout(flag)

    def end_bout(self):
        self.historys.append(self.current_bout.__dict__)
        self.dead = True

    def force_blind_bet(self, bet):
        self.current_bout.actions.append(('pre-flop', 'blind', bet))

    def set_hole_cards(self, cards):
        """
            player 得到两张底牌
        """
        self.current_bout.hole_cards = cards

    def update(self, jetton, money):
        """
            更新 player 信息
        """

        self.jetton = jetton
        self.money = money

    def action(self, stage, action, bet):
        if self.current_bout.fold or action == 'blind':
            return
        self.current_bout.actions.append((stage, action, bet-self.current_bout.pot))
        self.current_bout.pot = bet
        if action == 'fold':
            self.current_bout.fold = True

    def __str__(self):
        output_dict = dict(
            id=self.id,
            jetton=self.jetton,
            money=self.money,
            bout=self.current_bout,
        )
        return str(output_dict)

    def __repr__(self):
        return self.__str__()


class Round(object):
    """大局：随程序启动初始化"""
    def __init__(self, me_id):
        self.players = []
        self.started = False
        self.bout_count = 0
        self.me_id = me_id
        self.me = None

    def update(self, seat_info):
        if self.started:
            pass
        else:
            for s in seat_info:
                player = Player(player_id=int(s['pid']))
                self.players.append(player)
                if player.id == self.me_id:
                    self.me = player
            self.started = True

    def get_player_by_id(self, player_id):
        """通过 player_id 获取 player 对象"""
        for player in self.players:
            if player.id == player_id:
                return player
        return None

    def get_me(self):
        return self.me

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self.__dict__)


class Bout(object):
    """小局：seat 消息初始化"""

    def __init__(self, _round, seat_info):
        self.round = _round
        self.round.bout_count += 1
        self.community_cards = []       # 公共牌
        self.pot = 0                    # 彩池总额
        self.stage = 'pre-flop'
        self.small_blind = 0            # 小盲注
        # self.someone_all_in = False     # 有人 all_in
        for s in seat_info:
            player = self.round.get_player_by_id(int(s['pid']))
            player.begin_bout(jetton=int(s['jetton']), money=int(s['money']), flag=s['flag'])

    def force_blind_bet(self, player_id, bet):
        player = self.round.get_player_by_id(player_id)
        player.force_blind_bet(bet)

    def set_small_blind(self, num):
        self.small_blind = num

    def deal_hand(self, player_id, card_info):
        cards = []
        for c in card_info:
            card = Card(c['color'], c['point'])
            cards.append(card)

        player = self.round.get_player_by_id(player_id)
        player.set_hole_cards(cards)

    def update(self, pot, player_info):
        self.pot = pot
        for p in player_info:
            player = self.round.get_player_by_id(int(p['pid']))
            player.update(jetton=int(p['jetton']), money=int(p['money']))
            player.action(stage=self.stage, bet=int(p['bet']), action=p['action'])

    def add_community_cards(self, card_info):
        for c in card_info:
            card = Card(c['color'], c['point'])
            self.community_cards.append(card)

    def set_rank(self, player_id, rank, nut_hand):
        player = self.round.get_player_by_id(player_id)
        player.current_bout.rank = rank
        player.current_bout.nut_hand = nut_hand

    def __str__(self):
        output_dict = dict(
            community_cards=self.community_cards,
            pot=self.pot,
            players=self.round.players,
            small_blind=self.small_blind,
            stage=self.stage,
        )
        return str(output_dict)

    def __repr__(self):
        return self.__str__()

    def __del__(self):
        # 分析每个玩家
        for player in self.round.players:
            if player.dead:
                continue
            player.end_bout()

            if not player.current_bout.hole_cards:
                continue
            # pre-flop
            pre_flop = flop = turn = river = False
            for stage, action, bet in player.current_bout.actions:
                # 保存第一次动作
                if stage == 'pre-flop' and not pre_flop:
                    c1, c2 = player.current_bout.hole_cards
                    starting_hand = StartingHand(c1, c2)
                    result = starting_hand.evaluate()
                    # stage_dict = player.action_map.setdefault(stage, {})
                    # old_result = stage_dict.setdefault(action, result)
                    # stage_dict[action] = (old_result + result) / 2
                    player.action_map.setdefault(stage, {}).setdefault(action, []).append(result)
                    pre_flop = True
                elif stage == 'flop' and not flop:
                    hand = Hand(player.current_bout.hole_cards + self.community_cards[:3])
                    result = hand.evaluate()
                    # stage_dict = player.action_map.setdefault(stage, {})
                    # old_result = stage_dict.setdefault(action, result[0])
                    # stage_dict[action] = (old_result + result[0]) / 2
                    player.action_map.setdefault(stage, {}).setdefault(action, []).append(result[0])
                    flop = True
                elif stage == 'turn' and not turn:
                    hand = Hand(player.current_bout.hole_cards + self.community_cards[:4])
                    result = hand.evaluate()
                    # stage_dict = player.action_map.setdefault(stage, {})
                    # old_result = stage_dict.setdefault(action, result[0])
                    # stage_dict[action] = (old_result + result[0]) / 2
                    player.action_map.setdefault(stage, {}).setdefault(action, []).append(result[0])
                    turn = True
                elif stage == 'river' and not river:
                    hand = Hand(player.current_bout.hole_cards + self.community_cards[:5])
                    result = hand.evaluate()
                    # stage_dict = player.action_map.setdefault(stage, {})
                    # old_result = stage_dict.setdefault(action, result[0])
                    # stage_dict[action] = (old_result + result[0]) / 2
                    player.action_map.setdefault(stage, {}).setdefault(action, []).append(result[0])
                    river = True


if __name__ == '__main__':
    seat_info = [
        {'money': '1000', 'flag': 'button', 'pid': '4444', 'jetton': '3000'},
        {'money': '2000', 'flag': 'small blind', 'pid': '1111', 'jetton': '1000'},
        {'money': '0', 'flag': 'big blind', 'pid': '3333', 'jetton': '1000'},
        {'money': '2000', 'flag': None, 'pid': '2222', 'jetton': '5000'},
    ]

    r = Round()

    r.update(seat_info)
    b = Bout(r, seat_info)

    print r
    print b

    #
    # s.force_blind_bet(1111, 10)
    # print s
    #
    # card_info = [
    #     {'color': 'SPADES', 'point': '2'},
    #     {'color': 'HEARTS', 'point': '4'},
    # ]
    # s.deal_hand(1111, card_info)
    # print s
    #
    # player_info = [
    #     {'action': 'all_in', 'money': '0', 'jetton': '0', 'pid': '2222', 'bet': '1000'},
    #     {'action': 'raise', 'money': '2000', 'jetton': '7000', 'pid': '3333', 'bet': '1000'},
    #     {'action': 'raise', 'money': '0', 'jetton': '100', 'pid': '1111', 'bet': '900'},
    # ]
    # s.update(10, player_info)
    # print s
    #
    # card_info = [
    #     {'color': 'SPADES', 'point': 'A'},
    #     {'color': 'HEARTS', 'point': 'J'},
    #     {'color': 'HEARTS', 'point': 'Q'},
    # ]
    # s.add_community_cards(card_info)
    # print s
