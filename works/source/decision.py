# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
import random
# from collections import Counter

from hand import Hand, StartingHand

class Decision(object):

    def __init__(self, bout, debug=False):
        self.bout = bout
        self.debug = debug
        self._me = self.bout.round.get_me()

    def make(self):

        all_player_num = len(self.bout.round.players)
        active_players = [player for player in self.bout.round.players if not player.dead and not player.current_bout.fold]
        active_player_num = len(active_players)
        # print 'active_player_num:', active_player_num
        call_num = max([player.current_bout.pot for player in active_players]) - self._me.current_bout.pot  # 跟注金额
        # call_num = max([player.pot for player in self.seat.players if not player.fold]) - self.me.pot
        raise_num = self.bout.small_blind * 2      # 最小加注金额，小盲注的 2 倍
        print 'call_num:', call_num
        print 'raise_num:', raise_num
        # print self._me

        for player in active_players:
            player.current_bout.card_levels = range(0, 9)
            try:
                # c = Counter(player.action_map[self.bout.stage][player.current_bout.actions[-1][1]])
                seq = player.action_map[self.bout.stage][player.current_bout.actions[-1][1]]
                player.current_bout.card_levels = range(min(seq), max(seq))
                # level = c.most_common()[0][0]
                # print player.id, max(seq), min(seq)
                # print player.current_bout.hole_cards
                # print self.bout.community_cards
                # print '-----------------------'
            except KeyError as e:
                print e
            except IndexError as e:
                print e

        if random.randint(0, 9) < 8:
            return self.make_1(all_player_num=all_player_num, call_num=call_num, raise_num=raise_num)
        else:
            return self.make_3(active_players=active_players, all_player_num=all_player_num, call_num=call_num, raise_num=raise_num)
        #     return self.make_2(call_num=call_num)

        return self.make_1(all_player_num=all_player_num, call_num=call_num, raise_num=raise_num)

    def make_1(self, all_player_num, call_num, raise_num):
        decision = 'fold'
        level_fix_map = {
            2: -2, 3: -1,
            4: -1, 5: -1, 6: 0,
            7: 0, 8: 0,
        }
        # print 7 + level_fix_map.get(all_player_num, 0)
        # print 5 + level_fix_map.get(all_player_num, 0)
        # print 3 + level_fix_map.get(all_player_num, 0)

        # call_num 要限制 self.me.pot
        # aggression = 7
        # random_int = random.randint(0, 9)
        # random_int < aggression

        if len(self.bout.community_cards) < 3:
            c1, c2 = self._me.current_bout.hole_cards
            starting_hand = StartingHand(c1, c2)
            result = starting_hand.evaluate()
            # print 'starting hand evaluate:', result

            if call_num > self.bout.small_blind*2 * 10:
                if result >= 7 + level_fix_map.get(all_player_num, 0):
                    decision = 'call'
                    if self.debug:
                        print '试试运气，跟牌！'
                else:
                    decision = 'fold'
                    if self.debug:
                        print '一上来就拼命，弃牌！'
                # decision = 'fold'
                # if self.debug:
                #     print '一上来就拼命，弃牌！'
            elif call_num > self.bout.small_blind*2 * 5:
                if result >= 6:
                    decision = 'call'
                    if self.debug:
                        print '虽然押注很大，但是我的手牌好，跟牌！'
                else:
                    decision = 'fold'
                    if self.debug:
                        print '押注太大，况且我的手牌不好，弃牌！'
            elif call_num > 0:
                if result >= 7 + level_fix_map.get(all_player_num, 0):
                    if self._me.current_bout.pot < self.bout.small_blind*2 * 10:
                        decision = 'raise {raise_num}'.format(raise_num=call_num+raise_num*3)
                        if self.debug:
                            print '押注不大，而且我的手牌好，加注！'
                    else:
                        decision = 'call'
                        if self.debug:
                            print '押注太大，虽然我的手牌好，跟牌！'
                elif result >= 5 + level_fix_map.get(all_player_num, 0):
                    if self._me.current_bout.pot < self.bout.small_blind*2 * 5:
                        decision = 'call'
                        if self.debug:
                            print '押注不大，而且我的手牌一般，跟牌！'
                    else:
                        decision = 'fold'
                        if self.debug:
                            print '押注太大，而且我的手牌一般，弃牌！'
                elif result >= 3 + level_fix_map.get(all_player_num, 0):
                    if self._me.current_bout.flag and 'blind' in self._me.current_bout.flag:
                        if call_num <= self.bout.small_blind*2 * 3 and self._me.current_bout.pot < self.bout.small_blind*2 * 8:
                            decision = 'call'
                            if self.debug:
                                print '押注不大，我的手牌也一般，但是我是盲注，跟牌！'
                        else:
                            decision = 'fold'
                            if self.debug:
                                print '押注有点大，我的手牌也一般，即使我是盲注，弃牌！'
                    else:
                        decision = 'fold'
                        if self.debug:
                            print '押注不大，我的手牌也一般，我不是盲注，弃牌！'
                else:
                    # result < 2
                    if self._me.current_bout.flag and 'blind' in self._me.current_bout.flag:
                        if call_num <= self.bout.small_blind*2 and self._me.current_bout.pot < self.bout.small_blind*2 * 5:
                            decision = 'call'
                            if self.debug:
                                print '押注不大，我的手牌很烂，但是我是盲注，跟牌！'
                        else:
                            decision = 'fold'
                            if self.debug:
                                print '押注有点大，我的手牌很烂，即使我是盲注，弃牌！'
                    else:
                        decision = 'fold'
                        if self.debug:
                            print '押注不大，我的手牌超烂，我不是盲注，弃牌！'
            else:
                # call_num == 0
                decision = 'check'
                if self.debug:
                    print '没有人跟牌，让牌！'
        else:
            cards = self._me.current_bout.hole_cards + self.bout.community_cards
            hand = Hand(cards)
            result = hand.evaluate()
            # print 'hand evaluate:', result

            if result[0] >= 6:
                # decision = 'raise {raise_num}'.format(raise_num=raise_num*10)
                if self.debug:
                    print '押注不大，而且我的牌面超级好，加注！'
                decision = 'all_in'
                if self.debug:
                    print '押注不大，而且我的牌面超级好，ALLIN！'
            elif result[0] >= 5 + level_fix_map.get(all_player_num, 0):
                if call_num > self.bout.small_blind*2 * 15:
                    decision = 'call'
                    if self.debug:
                        print '押注太大，虽然我的牌面很好，跟牌！'
                elif call_num > 0:
                    decision = 'raise {raise_num}'.format(raise_num=call_num+raise_num*9)
                    if self.debug:
                        print '押注不大，而且我的牌面很好，加注！'
                else:
                    decision = 'check'
                    if self.debug:
                        print '押注不大，我的手牌很好，让牌！'

            elif result[0] >= 3 + level_fix_map.get(all_player_num, 0):

                if call_num > self.bout.small_blind*2 * 5:
                    decision = 'fold'
                    if self.debug:
                        print '押注有点大，我的手牌也一般，弃牌！'
                elif call_num > 0:
                    decision = 'call'
                    if self.debug:
                        print '押注不大，我的手牌一般，跟牌！'
                else:
                    decision = 'check'
                    if self.debug:
                        print '押注不大，我的手牌一般，让牌！'
            else:
                if call_num > self.bout.small_blind*2 * 2 and self._me.current_bout.pot > self.bout.small_blind*2 * 5:
                    decision = 'fold'
                    if self.debug:
                        print '押注太大，而且我的牌面不好，弃牌！'
                elif call_num > 0:
                    decision = 'call'
                    if self.debug:
                        print '押注不大，我的手牌不好，跟牌！'
                else:
                    decision = 'check'
                    if self.debug:
                        print '押注不大，而且我的牌面不好，让牌！'

        return decision

    def make_2(self, call_num):
        print 'oasis'

        if call_num + self._me.pot < self._me.money + self._me.jetton / 3:
            decision = 'call'
        else:
            decision = 'fold'

        return decision

    def make_3(self, active_players, all_player_num, call_num, raise_num):

        if len(self.bout.community_cards) < 3:
            c1, c2 = self._me.current_bout.hole_cards
            starting_hand = StartingHand(c1, c2)
            r = starting_hand.evaluate()
        else:
            cards = self._me.current_bout.hole_cards + self.bout.community_cards
            hand = Hand(cards)
            r = hand.evaluate()[0]

        try:
            max_level = max([max(player.current_bout.card_levels) for player in active_players])
        except ValueError as e:
            return self.make_1(all_player_num=all_player_num, call_num=call_num, raise_num=raise_num)

        if r > max_level:
            decision = 'raise {raise_num}'.format(raise_num=call_num+raise_num*3)
        elif r > max_level // 2:
            decision = 'call'
        else:
            decision = 'check'

        return decision





