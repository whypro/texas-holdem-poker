# -*- coding: utf-8 -*-
import socket
import time
import sys
import re

from round import Round, Bout
from decision import Decision


class GameClient(object):

    RECV_BUFSIZE = 4096
    player_name = 'Wolves'

    def __init__(self, server_ip, server_port, ip, port, player_id, debug=False):
        self.server_addr = (server_ip, server_port)
        self.local_addr = (ip, port)
        self.me_id = player_id
        self.debug = debug

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.recv_buf = ''          # socket 接收缓冲区
        self.msg_list = []          # 已分包消息缓冲区
        self.retry_interval = 3     # seconds

        self.round = Round(self.me_id)
        self.bout = None

    def run(self):
        self.sock.bind(self.local_addr)
        while True:
            try:
                self.sock.connect(self.server_addr)
            except socket.error as e:
                print '连接出错（{e}），{interval} 秒后重试'.format(e=e, interval=self.retry_interval)
                time.sleep(self.retry_interval)
            else:
                break

        self.send_reg_msg(self.me_id, self.player_name, need_notify=True)

        while True:
            self._recv()
            for data in self.msg_list:
                self.recv_msg(data)
            del self.msg_list[:]

        self.sock.close()

    def recv_msg(self, data):
        # print '========================'
        # print data
        # print '========================'
        if 'seat/' in data:
            self.recv_seat_msg(data)
        elif 'blind/' in data:
            self.recv_blind_msg(data)
        elif 'hold/' in data:
            self.recv_hold_cards_msg(data)
        elif 'inquire/' in data:
            self.recv_inquire_msg(data)
        elif 'flop/' in data:
            self.recv_flop_msg(data)
        elif 'turn/' in data:
            self.recv_turn_msg(data)
        elif 'river/' in data:
            self.recv_river_msg(data)
        elif 'showdown/' in data:
            self.recv_showdown_msg(data)
        elif 'pot-win/' in data:
            self.recv_pot_win_msg(data)
        elif 'game-over' in data:
            self.recv_game_over_msg(data)
        elif 'notify/' in data:
            self.recv_notify_msg(data)
        else:
            print 'unkown msg:', data

    def send_reg_msg(self, player_id, player_name, need_notify=False):
        """
            含义：player向server注册id和name信息，以及是否需要通知消息
            方向：player -> server
            方式：单播
            说明：若player希望收到通知消息（参见通知消息（notify-msg）），则在注册消息中填上”need_notify”。缺省不填则不会收到通知消息。
        """
        data = 'reg: {pid} {pname} {need_notify} \n'.format(pid=player_id, pname=player_name, need_notify='need_notify' if need_notify else '')
        self._send(data)

    def send_action_msg(self, action):
        """
            含义：在收到server的inquire消息后，player发出的行动消息
            方向：player –> server
            方式：单播
        """
        data = action
        self._send(data)

    def recv_seat_msg(self, data):
        """
            含义：各牌手的座次信息，即发牌和喊注次序
            方向：server -> player
            方式：广播
            说明：只剩下两个玩家时，将没有大盲注及后续座位信息
        """
        if self.debug:
            print '----NEW GAME----'
        seat_info = []
        pattern = re.compile(r'((?P<flag>.*):\s)?(?P<pid>\d+)\s(?P<jetton>\d+)\s(?P<money>\d+)\s\n')
        for match in pattern.finditer(data):
            if match:
                seat_info.append(match.groupdict())
        self.round.update(seat_info)
        self.bout = Bout(self.round, seat_info)

    def recv_blind_msg(self, data):
        """
            含义：server发布盲注玩家自动扣除盲注金额
            方向：server –> player
            方式：广播
            说明：大盲注金额为小盲注的2倍，只有2个玩家时，没有大盲注信息
        """
        blinds = []
        pattern = re.compile(r'(?P<pid>\d+): (?P<bet>\d+)')
        for match in pattern.finditer(data):
            if match:
                md = match.groupdict()
                self.bout.force_blind_bet(int(md['pid']), int(md['bet']))
                blinds.append(int(md['bet']))
        self.bout.set_small_blind(min(blinds))

    def recv_hold_cards_msg(self, data):
        """
            含义：server发2张手牌
            方向：server –> player
            方式：单播
            说明：
        """
        card_info = []
        pattern = re.compile(r'(?P<color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<point>[2-9]|10|J|Q|K|A)\s\n')
        for match in pattern.finditer(data):
            if match:
                card_info.append(match.groupdict())
        self.bout.deal_hand(self.me_id, card_info)

    def recv_inquire_msg(self, data):
        """
            含义：server向player询问行动决策
            方向：server –> player
            方式：单播
            说明：
                player只有收到此询问消息之后, 才能发出action消息
                询问消息中会包含如下内容：
                    a)  本手牌已行动过的所有玩家（包括被询问者和盲注）的手中筹码、剩余金币数、本手牌累计投注额、及最近的一次有效action，按逆时针座次由近及远排列，上家排在第一个
                    b)  当前底池总金额（本轮投注已累计）

        """
        # print data
        player_info = []
        pot = 0

        pattern = re.compile(r'(?P<pid>\d+)\s(?P<jetton>\d+)\s(?P<money>\d+)\s(?P<bet>\d+)\s(?P<action>blind|check|call|raise|all_in|fold)\s\n')
        for match in pattern.finditer(data):
            if match:
                player_info.append(match.groupdict())
                # if match.groupdict()['action'] == 'all_in':
                #     self.seat.someone_all_in = True
        pattern = re.compile(r'total pot: (\d+)')
        match = pattern.search(data)
        if match:
            pot = int(match.group(1))

        self.bout.update(pot, player_info)

        # 决策
        # print self.seat
        begin_time = time.time()
        decision = Decision(self.bout)
        end_time = time.time()
        print (end_time - begin_time) * 1000
        self.send_action_msg(decision.make())

    def recv_flop_msg(self, data):
        """
            含义：server发出三张公共牌
            方向：server –> player
            方式：广播
        """
        card_info = []
        pattern = re.compile(r'(?P<color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<point>[2-9]|10|J|Q|K|A)\s\n')
        for match in pattern.finditer(data):
            if match:
                card_info.append(match.groupdict())

        self.bout.add_community_cards(card_info)
        self.bout.stage = 'flop'

    def recv_turn_msg(self, data):
        """
            含义：server发出一张转牌
            方向：server –> player
            方式：广播
        """
        pattern = re.compile(r'(?P<color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<point>[2-9]|10|J|Q|K|A)\s\n')
        match = pattern.search(data)
        if match:
            self.bout.add_community_cards([match.groupdict()])
        self.bout.stage = 'turn'

    def recv_river_msg(self, data):
        """
            含义：server发出一张转牌
            方向：server –> player
            方式：广播
        """
        pattern = re.compile(r'(?P<color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<point>[2-9]|10|J|Q|K|A)\s\n')
        match = pattern.search(data)
        if match:
            self.bout.add_community_cards([match.groupdict()])
        self.bout.stage = 'river'

    def recv_showdown_msg(self, data):
        """
            含义：摊牌消息（含5张公共牌、被摊牌选手的2张手牌及其最佳手牌牌型）
            方向：server –> player
            方式：广播
            说明：只有2人或2人以上未盖牌时，server才发出摊牌消息，盖牌玩家的底牌不被公布
        """
        # print data
        # 5 张公共牌
        pattern = re.compile(r'(?P<color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<point>[2-9]|10|J|Q|K|A)\s\n')
        for match in pattern.finditer(data):
            if match:
                pass
                # print match.groupdict()

        # 每位玩家的 2 张底牌
        pattern = re.compile(
            r'(?P<rank>\d):\s(?P<pid>\d+)\s'
            r'(?P<card_1_color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<card_1_point>[2-9]|10|J|Q|K|A)\s'
            r'(?P<card_2_color>SPADES|HEARTS|CLUBS|DIAMONDS)\s(?P<card_2_point>[2-9]|10|J|Q|K|A)\s'
            r'(?P<nut_hand>HIGH_CARD|ONE_PAIR|TWO_PAIR|THREE_OF_A_KIND|STRAIGHT|FLUSH|FULL_HOUSE|FOUR_OF_A_KIND|STRAIGHT_FLUSH)'
            r'\s\n'
        )
        for match in pattern.finditer(data):
            if match:
                mg = match.groupdict()
                player_id = int(mg['pid'])
                card_info = [
                    {'color': mg['card_1_color'], 'point': mg['card_1_point']},
                    {'color': mg['card_2_color'], 'point': mg['card_2_point']},
                ]
                self.bout.deal_hand(player_id, card_info)
                self.bout.set_rank(player_id, rank=int(mg['rank']), nut_hand=mg['nut_hand'])
        self.bout.stage = 'showdown'

    def recv_pot_win_msg(self, data):
        """
            含义：server公布彩池分配结果
            方向：server –> player
            方式：广播
            说明：如果所有玩家都弃牌，彩池金额全部丢弃

        """
        # print data
        pot_info = []
        pattern = re.compile(r'(?P<pid>\d+):\s(?P<num>\d+)')
        for match in pattern.finditer(data):
            if match:
                pot_info.append(match.groupdict())

        # self.seat.distribute_pot(pot_info)
        # print self.bout
        del self.bout

    def recv_game_over_msg(self, data):
        """
            含义：游戏结束
            方向：server –> player
            方式：广播
            说明：player收到此消息后应当先关闭与server的socket连接，再退出程序。
        """
        print 'game-over'

        # import json
        #
        # with open('history.json', 'w') as f:
        #     histories = []
        #     for player in self.round.players:
        #         histories.append(player.historys)
        #     json.dump(histories, f)
        print self.round.me.action_map
        self.sock.close()
        sys.exit(0)

    def recv_notify_msg(self, data):
        """
            含义：server向无需行动的player通知其它玩家的最新动态
            方向：server –> player
            方式：单播
            说明：
                只有在注册时指定了need_notify选项（参见注册消息（reg-msg） ），才会收到通知消息
                当玩家在一局比赛中已无需行动（盖牌或All in后），牌桌会在该局比赛结束前，继续用notify消息推送其它玩家的动态
                通知消息内容和询问消息相同，即：
                    a)  本手牌已行动过的所有玩家（包括被询问者和盲注）的手中筹码、剩余金币数、本手牌累计投注额、及最近的一次有效action，按逆时针座次由近及远排列，上家排在第一个
                    b)  当前底池总金额（本轮投注已累计）
                通知消息不需要任何响应

        """
        # print data
        player_info = []
        pot = 0

        pattern = re.compile(r'(?P<pid>\d+)\s(?P<jetton>\d+)\s(?P<money>\d+)\s(?P<bet>\d+)\s(?P<action>blind|check|call|raise|all_in|fold)\s\n')
        for match in pattern.finditer(data):
            if match:
                player_info.append(match.groupdict())
                # if match.groupdict()['action'] == 'all_in':
                #     self.seat.someone_all_in = True
        pattern = re.compile(r'total pot: (\d+)')
        match = pattern.search(data)
        if match:
            pot = int(match.group(1))

        self.bout.update(pot, player_info)

    def _send(self, data):
        if self.debug:
            print 'Send <{data}>'.format(data=data.rstrip())
        self.sock.send(data)

    def _recv(self):
        pattern = re.compile(r'(?P<data>((\w+(\-\w+)?)/.*/(\3)|game-over)\s\n)', re.S)

        buf = self.sock.recv(self.RECV_BUFSIZE)
        if buf:
            # print buf
            self.recv_buf += buf
            # 匹配
            for match in pattern.finditer(self.recv_buf):
                if match:
                    self.msg_list.append(match.groupdict()['data'])
            # 将已匹配的字符串从缓冲中删除
            self.recv_buf = pattern.sub('', self.recv_buf)
        else:
            print 'connection closed.'
            self.sock.close()
            sys.exit(0)


if __name__ == '__main__':
    # python game.py [服务器IP] [服务器PORT] [客户端IP] [客户端PORT] [玩家ID]

    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])
    local_ip = sys.argv[3]
    local_port = int(sys.argv[4])
    player_id = int(sys.argv[5])

    gc = GameClient(server_ip, server_port, local_ip, local_port, player_id, debug=True)
    gc.run()
