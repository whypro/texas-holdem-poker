#!/usr/bin/env bash
for i in 1 2 3 4
do
    export "PLAYER"$i"_IP"=127.0.0.1
    export "PLAYER"$i"_PORT"=600$i
    export "PLAYER"$i"_ID"=$i$i$i$i
done
cd server
rm replay.txt
./gameserver -gip 127.0.0.1 -seq replay -d 1 -r 5
cd ..
